<?php
namespace Admin\Controller;
//use Admin\Controller\CommonController;
class JobController extends CommonController {

    public function indexAction(){
        $model = M();
        $sql = "select * from jobs";
        $jobData = $model->query($sql);
        $this->assign('jobData',$jobData);
        $this->display('index');
    }

    /**
     * 编辑职位
     */
    public function editAction(){
        $jobList = I('post.job_list');
        foreach($jobList as $id => $v){
            $model = M();
            $sql = "update jobs set name ='{$v}' where id = '{$id}'";
            $model->execute($sql);
        }
        $this->success('修改成功',U("Admin/Job/index"));

    }

    /**
     * 删除职位
     */
    public function deleteAction(){
        $id = I('get.id');
        $model = M();
        $sql = "delete from jobs where id = '{$id}'";
        $model->execute($sql);

        $this->success('删除成功',U("Admin/Job/index"));

    }

    /**
     * 添加职位
     */
    public function addAction(){
        $jobData = I('post.jobs');
        if(empty($jobData)){
            $this->display('add');
        }else{
            $Arr = array();
            $Arr = explode("\n",$jobData);
            foreach($Arr as $v){
                $v = trim($v);
                $model = M();
                $sql = "insert into jobs(name) values('$v')";
                $model->execute($sql);
            }
            $this->success('添加成功',U("Admin/Job/index"));
        }
    }
}