<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;
class IndexController extends CommonController {

    public function indexAction(){

        $this->display('index');
    }

    public function questionAction()
    {
    	$Model = M();

    	$sql = "select count(*) num,FROM_UNIXTIME(create_time,'%m-%d') as day from question WHERE create_time>UNIX_TIMESTAMP(date_sub(curdate(), INTERVAL 30 DAY)) and create_time< UNIX_TIMESTAMP(curdate()) group by day ORDER BY day desc limit 7";

    	$res = $Model->query($sql);

    	$res = json_encode($res);

    	echo $res;
    }
}