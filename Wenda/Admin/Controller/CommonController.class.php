<?php
namespace Admin\Controller;
use Think\Controller;

class CommonController extends Controller {

    public  function _initialize(){
        if(!isset($_SESSION['username'])){
            $this->error('还没有登录，请先登录在进行访问',U('Admin/Login/Index'),2);
        }

    }

}