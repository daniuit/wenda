<?php
namespace Admin\Controller;
//use Admin\Controller\CommonController;
class QuestionController extends CommonController {
    /**
     * 问题管理
     */
    public function indexAction(){
        $question = M('question'); // 实例化User对象
        $count = $question->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,8);// 实例化分页类 传入总记录数和每页显示的记录数(6)
        $show = $Page->show();// 分页显示输出
        $this->assign('page',$show);

        $model = M();
        $sql = "select t1.title,t1.reply,t1.attention,t1.views,t1.id,t2.username,t1.create_time,t1.update_time from question t1 left join user t2 on t1.uid = t2.id order by t1.create_time desc limit {$Page->firstRow},{$Page->listRows}";
        $rtnQuestion = $model -> query($sql);
        $this->assign('rtnQuestion',$rtnQuestion);

        $this->display('index');
    }

    /**
     * 删除问题
     */
    public function deleteAction(){
        $id = I('get.id');
        $model = M();
        $sql = "delete from question where id = '{$id}'";
        $model->execute($sql);

        $this->success('删除成功',U("Admin/Question/index"));
    }

    public function searchAction(){
        $this->display('search');
    }
}