<?php
namespace Admin\Controller;
//use Admin\Controller\CommonController;
use Common\Common\Tree;
class CategoryController extends CommonController {
    /**
     * 分类列表
     */
    public function indexAction(){
        $model = M();
        $sql = "select * from  category";
        $cateData = $model->query($sql);

        $tree = new tree();
        $resdata = $tree->categorytree($cateData);

        $this->assign('cateData',$resdata);
        $this->display('index');

    }

    /**
     *添加分类
     */
    public function addAction(){
        if(IS_POST){
            $category_name = I('post.category_name');
            $fid = I('post.fid');
            $sql = "Insert into category (category_name,fid) values('{$category_name}','{$fid}')";

            $model = M();

            $model->execute($sql);

            $this->success('添加成功',U('Admin/category/index'),2);

        }else{
            $model = M();
            $sql = "select * from  category";
            $cateData = $model->query($sql);
            $tree = new tree();
            $resdata = $tree->categorytree($cateData);

            $this->assign('cateData', $resdata);
            $this->display('add');
        }
    }

    /**
     *编辑分类
     */
    public function editAction(){
        $id = I('get.id');
        $model = M();
        $sql = "select * from category where id = '{$id}'";
        $cateData = $model->query($sql);
        $this->assign('cateData',$cateData);

        $this->display('edit');
    }

    /**
     *更新分类到数据库
     */
    public function updateAction(){
        $category_name = I('post.category_name');
        $id = I('post.id');
        $model = M();
        $sql = "update category set category_name = '{$category_name}' where id = '{$id}'";
        if($model->execute($sql)){
            $this->success('修改成功',U('Admin/Category/index'),2);
        }
    }
}