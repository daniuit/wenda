<?php
namespace Admin\Controller;
use Think\Controller;
class UserController extends Controller {

    public function indexAction(){
        $this->display('index');
    }

    public function addAction(){
        $this->display('add');
    }

    public function editAction(){
        $this->display('edit');
    }
}