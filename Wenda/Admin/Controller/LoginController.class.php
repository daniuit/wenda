<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Verify;
class LoginController extends Controller {

    public function indexAction(){
        if (IS_POST) {
            $username = I('post.username');
            $password = md5(I('post.password'));
            $Model = M();
            $userNamerInfo = $Model->query("SELECT * FROM user where username='{$username}'");
            $userMailInfo = $Model->query("SELECT * FROM user where mail='{$username}'");

            if(!empty($userNamerInfo) || !empty($userMailInfo)){

                $userInfo = empty($userNamerInfo) ? $userMailInfo[0] : $userNamerInfo[0];

                if($userInfo['password']==$password && $userInfo['gid']==1){
                    $_SESSION['username'] = $userInfo['username'];
                    $_SESSION['uid'] = $userInfo['id'];
                    $_SESSION['gid'] = $userInfo['gid'];
                    $_SESSION['face_url'] = $userInfo['face_url'];
                    $this->success('登录成功',U("index/index"),2);
                }else{
                    $this->error('用户名或密码不正确或者没有权限','',2);
                }

            }else{
                $this->error('用户名或密码不正确','',2);
            }

        } else {
            $this->display('index');
        }


    }



    public function logoutAction(){
        session_unset();
        session_destroy();
        $this->success('退出成功',U("Login/index"),2);
    }

//    /**
//    生成验证码类
//     **/
//    public function CodeAction(){
//        $config = array(
//            'imageW'=>120,
//            'imageH'=>40,
//            'fontSize'=>16,
//            'length'=>1
//        );
//        $Verify = new Verify($config);
//        $Verify->entry();
//    }
//
//    /**
//    验证用户提交的验证码
//     **/
//    public function check_verify($code, $id = ''){
//        $verify = new Verify();
//        return $verify->check($code, $id);
//    }

}