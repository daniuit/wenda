<?php
namespace Index\Controller;
use Think\Controller;

class LoginController extends Controller{
   public function indexAction(){

      if (IS_POST) {
         $username = I('post.user_name');
         $password = md5(I('post.password'));
         $Model = M();
         $isName= $Model->query("select * from user where username ='{$username}'  and password ='{$password}'");
         $isMail = $Model->query("select * from user where mail ='{$username}'  and password ='{$password}'");

         if (empty($isName) && empty($isMail)) {
            $this->error('用户名或密码错误', '', 2);
         }else{
            $userInfo = empty($isName) ? $isMail[0] : $isName[0];
            $_SESSION['username'] =  $userInfo['username'];
            $_SESSION['uid'] = $userInfo['id'];
            $_SESSION['gid'] = $userInfo['gid'];
            $_SESSION['face_url'] = $userInfo['face_url'];
            session_start();
            setcookie(session_name(),session_id(),time()+60);
            $this->success('登录成功', U("Index/index"), 2);
         }
      } else {
         $this->display('index');
      }

   }

   public function logoutAction(){
      session_unset();
      session_destroy();
      $this->success('退出成功',U("Index/index"),2);
   }
}
