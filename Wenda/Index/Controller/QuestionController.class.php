<?php
namespace Index\Controller;
use Think\Controller;
class QuestionController extends Controller {
    /**
     *发问页面展示
     */
    public function indexAction(){
        $model = M();
        $sqlCategory = "select * from category";
        $category= $model->query($sqlCategory);
        $this->assign('category',$category);


        if(IS_POST){

            $title = I('post.question_content');
            $cid = I('post.category');
            $content = I('post.content');
            $uid = $_SESSION['uid'];
            $create_time = time();
            if(empty($title) || empty($cid) || empty($content)){
                $this->error('内容或者标题或者分类不能留空',U("Index/question/index"),2);
            }


            $sql = "insert into question (title,cid,content,uid,create_time) VALUES('{$title}','{$cid}','{$content}','{$uid}','{$create_time}')";
            $model->execute($sql);
            //获取最后一条插入的ID
            $id = $model->getLastInsID();

//            $sqlId = "select id from question where uid = $uid order by id desc limit 1";
//            $id = $model->query($sqlId);
//            var_dump($id);
            $this->success('发表成功',U("Index/question/show/id/{$id}"),2);


        }else{
            $this->display('index');
        }
    }

    /**
     *百度编辑器
     */
    public function ueditorAction(){
        $data = new \Org\Util\Ueditor();
        echo $data->output();
    }

    /**
     *问题页面展示
     */
    public function showAction(){
        $id = I('get.id');
        $uid = I('session.uid');
        $model = M();

        //关注人的数量
        $sqlFocus = "select count(*) as count from focus where qid = $id ";
        $focus = $model->query($sqlFocus);
        $focus = $focus[0]['count'];
        $this->assign('focus',$focus);

        //判断是否关注
        $sql = "select * from focus where qid = $id and uid = $uid ";
        $exist = $model -> query($sql);
        $exist = empty($exist)?0:1;
        $this->assign('exist',$exist);

        //更新浏览次数
        $sql = "select t1.views from question t1 inner join user t2 on t1.uid = t2.id and t1.id ='{$id}'";
        $view = $model -> query($sql);
        $view = $view[0]['views'] + 1;
        $sqlView = "update question set views ='$view' where id='{$id}' ";
        $model->execute($sqlView);

//        if(empty($question)){
//            $this->error('用户不存在',$_SERVER['HTTP_REFERER'],5);
//        }




        //问题展示
        $sql = "select t1.views,t1.cid,t1.id,t1.title,t1.content,t2.username,t2.face_url from question t1 inner join user t2 on t1.uid = t2.id and t1.id ='{$id}'";
        $question = $model->query($sql);
        $question = $question[0];
        $question['content'] = htmlspecialchars_decode($question['content']);
        $this->assign('question',$question);



        //回复展示
        $sqlAnswer = "select t1.qid,t1.create_time,t1.uid,t2.face_url,t1.content,t2.username from answer t1 inner join user t2 on t1.uid = t2.id and t1.qid ='{$id}'";

        $answer = $model->query($sqlAnswer);
        $this->assign('answer',$answer);

        $answerCount = count($answer);

        $this->assign('count',$answerCount);



        $this->display('show');
    }

    /**
     * 问题编辑
     */
    public function editAction(){
        $id = I('get.id');
        $model = M();
        $sql = "select title,content,cid from question where id = '{$id}'";
        $question = $model->query($sql);
        $question = $question[0];
        $question['content'] = htmlspecialchars_decode($question['content']);
        $this->assign('question',$question);
        $sql = "select * from category";
        $categoryInfo = $model->query($sql);
        $this->assign('categoryInfo',$categoryInfo);

        if(IS_POST) {

            $title = I('post.question_content');
            $cid = I('post.category');
            $content = I('post.content');
            $update_time = time();
            if (empty($title) || empty($cid) || empty($content)) {
                $this->error('内容或者标题或者分类不能留空', U("Index/question/edit/id/$id"), 2);
            }


            $sql = "update question set title = '{$title}', cid = '{$cid}', content = '{$content}', update_time = '{$update_time}' where id = '{$id}'";


            if($model->execute($sql)){
                $this->success('编辑成功', U("Admin/Question/index"),2);
            }
        }

        $this->display('edit');
    }
}