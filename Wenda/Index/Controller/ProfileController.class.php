<?php
namespace Index\Controller;
use Index\Controller\CommonController;
class ProfileController extends CommonController
{
    //个人简介
    public function indexAction()
    {
        $id = $_SESSION['uid'];
        $model = M();
        $sql = "select * from user where id = '{$id}'";
        $userInfo = $model->query($sql);
        $userInfo = $userInfo[0];
        $userInfo['birthday'] = $this->_changeDate($userInfo['birthday']);

        $sql = "select * from jobs";
        $jobsInfo =  $model->query($sql);
        $this->assign('userInfo',$userInfo);
        $this->assign('jobsInfo',$jobsInfo);
        $this->display('index');

    }


    //密码安全设置
    public function securityAction()
    {
        if(IS_POST){

            $id = $_SESSION['uid'];
            $postData = I('post.');
            if(empty($postData['password']) || empty($postData['re_password']) || empty($postData['old_password']) ){
                $this->error('密码不能留空',U('Index/profile/security'),2);
            }
            if($postData['password'] != $postData['re_password']){
                $this->error('新密码不一致',U('Index/profile/security'),2);
            }

            $model = M();
            $sql = "select * from user where id = '{$id}'";
            $userInfo = $model->query($sql);
            $userInfo = $userInfo[0];
            $newPostdata = md5($postData['password']);
            $oldPostdata = md5($postData['old_password']);
            if($oldPostdata != $userInfo['password']){
                $this->error('你输入的旧密码不正确',U('Index/profile/security'),2);
            }else if($newPostdata == $userInfo['password']){
                $this->error('新密码不能与旧密码相同',U('Index/profile/security'),2);
            }else{
                $sql = "update user set password = '{$newPostdata}' where id = '{$id}'";
                $model->execute($sql);
                $this->success('修改密码成功',U('Index/login/logout'),2);
            }

        }else{
            $this->display('security');
        }


    }

    // 上传头像
    public function uploadAction()
    {
        $this->display('upload');
    }
    //日期改成年月日
    private function _changeDate($date)
    {
        $arr = explode('-', $date);
        $dateArr = array();
        $dateArr['birthday_y'] = $arr[0];
        $dateArr['birthday_m'] = $arr[1];
        $dateArr['birthday_d'] = $arr[2];
        return $dateArr;
    }
    //更新到数据库
    public function updateAction(){
        if(IS_POST){

            $id = $_SESSION['uid'];
            $postData = I('post.');

            $birthday = $postData['birthday_y'].'-'.$postData['birthday_m'].'-'.$postData['birthday_d'];

            $sql= "update user set turename='{$postData['turename']}',sex='{$postData['sex']}',birthday='{$birthday}',job='{$postData['job']}',city='{$postData['city']}',province='{$postData['province']}',introduce='{$postData['introduce']}',qq='{$postData['qq']}',phone='{$postData['phone']}',mail='{$postData['mail']}',site='{$postData['site']}' where id='{$id}' ";

//            header("Content-type: text/html; charset=utf-8");
//            var_dump($sql);

            $modle = M();

            $modle->execute($sql);

            $this->success('修改成功','index',2);

        }else{
            $this->error('非法请求','index',2);
        }
    }

    //上传到空间
    public function meichuanAction(){

        $id = $_SESSION['uid'];
        $save_path = './Uploads/avatars/'.date('Ymd',time());    //定义一个要上传头像的目录
        is_dir($save_path) || mkdir($save_path,0777,TRUE);    //如果没有这么目录,那么就创建这个目录

        if(is_uploaded_file($_FILES['upload_file']['tmp_name'])){
            $uniqid = uniqid();
            $filename = $save_path . '/' . $uniqid . '.jpg';
            move_uploaded_file($_FILES['upload_file']['tmp_name'],$filename);

            if (is_file($filename) ) {
                $filename = substr($filename,1);

                $model = M();

                $sql = "update user set face_url='$filename' where id='{$id}' ";

                $model->execute($sql);

                $_SESSION['face_url']=$filename;


                echo '上传头像成功!';
                exit ();
            }else {
                die ( '上传头像失败!' );
            }

        }else{
            echo '你的操作不合法';
        };
    }

}