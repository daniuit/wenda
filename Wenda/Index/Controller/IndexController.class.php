<?php
namespace Index\Controller;
use Think\Controller;
class IndexController extends Controller {
    /**
     * 最新话题
     */
    public  function indexAction(){
        $question = M('question'); // 实例化User对象
        $count = $question->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,6);// 实例化分页类 传入总记录数和每页显示的记录数(6)
        $show = $Page->show();// 分页显示输出
        $this->assign('page',$show);
        $model = M();

        $sql = "select t1.attention,t1.reply,t1.id,t1.uid,t1.title,t1.views,t1.create_time,t2.username,t2.face_url,t3.category_name from question t1 left join user t2 on t1.uid = t2.id left join category t3 on t3.id = t1.cid order by create_time desc limit {$Page->firstRow},{$Page->listRows}";


        $questionData = $model->query($sql);
        $this->assign('questionData',$questionData);

        //热门话题
        $sql = "select t1.id,t1.uid,t1.title,t1.attention,t2.face_url from question t1 left join user t2 on t1.uid = t2.id order by attention desc limit 5";
        $popQuestion = $model->query($sql);
        $this->assign('popQuestion',$popQuestion);

        //热门用户
        $sql = "select count(*) cnt,t1.uid,t2.username,t2.face_url from answer t1 left join user t2 on t1.uid = t2.id group by t1.uid order by cnt desc limit 5";
        $maxAnswer = $model->query($sql);
        $this->assign('maxAnswer',$maxAnswer);

        $this->display('index');

    }


//
//    public function showAction(){
//        $User = M('User'); // 实例化User对象
//        $count      = $User->where('status=1')->count();// 查询满足要求的总记录数
//        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
//        $show       = $Page->show();// 分页显示输出
//        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
//        $list = $User->where('status=1')->order('create_time')->limit($Page->firstRow.','.$Page->listRows)->select();
//        $this->assign('list',$list);// 赋值数据集
//        $this->assign('page',$show);// 赋值分页输出
//        $this->display(); // 输出模板
//    }


}