<?php
namespace Index\Controller;
use Think\Controller;
use Think\Verify;
class RegisterController extends Controller {
   public function indexAction(){

      if(IS_POST){
         $username = I('post.user_name');
         $mail = I('post.email','','validate_email');
         $password = md5(I('post.password'));
         $verify = I('post.seccode_verify');

         //校对验证码
         if(!$this->check_verify($verify)) $this->error('验证码不正确','',2);

         // 判断邮箱
         if (empty($mail)) $this->error('邮箱格式不正确','',2);

         // 判断名是否存在

         //或者使用M快捷方法是等效的
         $Model = M();
         // //进行原生的SQL查询
         $isExist = $Model->query("SELECT * FROM user where username='{$username}'");

         if (!empty($isExist)) $this->error('用户已存在','',2);

         $sql = "insert into user (username,password,mail) VALUES('{$username}','{$password}','{$mail}')";

         if($Model->execute($sql)){
            $this->success('注册成功',U('Index/Index/index'),2);
         } ;


      }else{
         $this->display('index');
      }
   }


   /**
   生成验证码类
    **/
   public function CodeAction(){
      $config = array(
          'imageW'=>120,
          'imageH'=>40,
          'fontSize'=>16,
          'length'=>2
      );
      $Verify = new Verify($config);
      $Verify->entry();
   }

   /**
   验证用户提交的验证码
    **/
   public function check_verify($code, $id = ''){
      $verify = new Verify();
      return $verify->check($code, $id);
   }

}