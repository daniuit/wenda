<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<!-- saved from url=(0031)http://127.0.0.1/wenda/?/admin/ -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="blank">
    <meta name="format-detection" content="telephone=no">
    <title>概述 -学并思问答</title>
    <!--<base href="http://127.0.0.1/wenda/?/">--><base href=".">
    <link rel="stylesheet" type="text/css" href="/rj/Public/Admin/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/rj/Public/Admin/Css/icon.css" />
    <link rel="stylesheet" type="text/css" href="/rj/Public/Admin/Css/common.css" />


    <script type="text/javascript">
        var G_INDEX_SCRIPT = "?/";
        var G_BASE_URL = "http://127.0.0.1/wenda/?";
        var G_STATIC_URL = "http://127.0.0.1/wenda/static";
        var G_UPLOAD_URL = "http://127.0.0.1/wenda/uploads";
        var G_USER_ID = "1";
        var G_POST_HASH = "";
    </script>
    <script type="text/javascript" src="/rj/Public/Admin/Js/jquery-2.js"></script>
    <script type="text/javascript" src="/rj/Public/Admin/Js/aws_admin.js"></script>
    <script type="text/javascript" src="/rj/Public/Admin/Js/aws_admin_template.js"></script>
    <script type="text/javascript" src="/rj/Public/Admin/Js/jquery-form.js"></script>
    <script type="text/javascript" src="/rj/Public/Admin/Js/framework.js"></script>
    <script type="text/javascript" src="/rj/Public/Admin/Js/global.js"></script>
    <script type="text/javascript" src="/rj/Public/Admin/Js/echarts-data.js"></script>
    <script type="text/javascript" src="/rj/Public/Admin/Js/echarts.js"></script>



    <!--[if lte IE 8]>
    <script type="text/javascript" src="http://127.0.0.1/wenda/static/js/respond.js"></script>
    <![endif]-->
    <script src="http://echarts.baidu.com/build/dist/echarts.js"> </script>
</head>

<body>
<div class="aw-header">
    <button class="btn btn-sm mod-head-btn pull-left">
        <i class="icon icon-bar"></i>
    </button>

    <div class="mod-header-user">
        <ul class="pull-right">

            <li class="dropdown">
                <a href="http://127.0.0.1/wenda/?/#" class="dropdown-toggle mod-bell" data-toggle="dropdown">
                    <i class="icon icon-bell"></i>
                </a>
                <ul class="dropdown-menu mod-chat">
                    <p>没有通知</p>
                </ul>
            </li>

            <li class="dropdown username">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/rj<?php echo (session('face_url')); ?>" class="img-circle" width="30"><?php echo (session('username')); ?>
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu pull-right mod-user">
                    <li>
                        <a href="http://127.0.0.1/wenda" target="_blank"><i class="icon icon-home"></i>首页</a>
                    </li>

                    <li>
                        <a href="<?php echo U('Admin/index/index');?>"><i class="icon icon-ul"></i>概述</a>
                    </li>

                    <li>
                        <a href="<?php echo U('Admin/Login/logout');?>"><i class="icon icon-logout"></i>退出</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<div class="aw-side ps-container" id="aw-side">
    <div class="mod">
        <div class="mod-logo">
            <img class="pull-left" src="/rj/Public/Admin/images/logo.png" alt="">
            <h1>WeCenter</h1>
        </div>

        <div class="mod-message">
            <div class="message">
                <a class="btn btn-sm" href="http://127.0.0.1/wenda" target="_blank" title="首页">
                    <i class="icon icon-home"></i>
                </a>

                <a class="btn btn-sm" href="./images/index.html" title="概述">
                    <i class="icon icon-ul"></i>
                </a>

                <a class="btn btn-sm" href="<?php echo U('Admin/Login/logout');?>" title="退出">
                    <i class="icon icon-logout"></i>
                </a>
            </div>
        </div>

        <ul class="mod-bar">
            <input type="hidden" id="hide_values" val="0">
            <li>
                <a href="<?php echo U('Admin/Index/index');?>" class=" icon icon-home active">
                    <span>概述</span>
                </a>
            </li>
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-setting" data="icon">-->
                    <!--<span>全局设置</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-site">-->
                            <!--<span>站点信息</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-register">-->
                            <!--<span>注册访问</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-functions">-->
                            <!--<span>站点功能</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-contents">-->
                            <!--<span>内容设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-integral">-->
                            <!--<span>威望积分</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-permissions">-->
                            <!--<span>用户权限</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-mail">-->
                            <!--<span>邮件设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-openid">-->
                            <!--<span>开放平台</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-cache">-->
                            <!--<span>性能优化</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-interface">-->
                            <!--<span>界面设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <li>
                <a href="javascript:;" class=" icon icon-reply" data="icon">
                    <span>内容管理</span>
                </a>

                <ul class="hide">
                    <li>
                        <a href="<?php echo U('Admin/Question/index');?>">
                            <span>问题管理</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo U('Admin/Category/index');?>">
                            <span>回复管理</span>
                        </a>
                    </li>
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/article/list/">-->
                            <!--<span>文章管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/topic/list/">-->
                            <!--<span>话题管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <li>
                        <a href="<?php echo U('Admin/Category/index');?>">
                        <span>分类管理</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" class=" icon icon-user" data="icon">
                    <span>用户管理</span>
                </a>

                <ul class="hide">
                    <li>
                        <a href="<?php echo U('Admin/User/index');?>">
                            <span>用户列表</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://127.0.0.1/wenda/?/admin/user/group_list/">
                            <span>用户组</span>
                        </a>
                    </li>
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/invites/">-->
                            <!--<span>批量邀请</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <li>
                        <a href="<?php echo U('Admin/Job/index');?>">
                            <span>职位设置</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-report" data="icon">-->
                    <!--<span>审核管理</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/approval/list/">-->
                            <!--<span>内容审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/verify_approval_list/">-->
                            <!--<span>认证审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/register_approval_list/">-->
                            <!--<span>注册审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/question/report_list/">-->
                            <!--<span>用户举报</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-signup" data="icon">-->
                    <!--<span>内容设置</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/nav_menu/">-->
                            <!--<span>导航设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/category/list/">-->
                            <!--<span>分类管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/feature/list/">-->
                            <!--<span>专题管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/page/">-->
                            <!--<span>页面管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/help/list/">-->
                            <!--<span>帮助中心</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-share" data="icon">-->
                    <!--<span>微信微博</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/accounts/">-->
                            <!--<span>微信多账号管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/mp_menu/">-->
                            <!--<span>微信菜单管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/reply/">-->
                            <!--<span>微信自定义回复</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/third_party_access/">-->
                            <!--<span>微信第三方接入</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/qr_code/">-->
                            <!--<span>微信二维码管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/sent_msgs_list/">-->
                            <!--<span>微信消息群发</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weibo/msg/">-->
                            <!--<span>微博消息接收</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/receiving_list/">-->
                            <!--<span>邮件导入</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-inbox" data="icon">-->
                    <!--<span>邮件群发</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/tasks/">-->
                            <!--<span>任务管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/groups/">-->
                            <!--<span>用户群管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-job" data="icon">-->
                    <!--<span>工具</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/tools/">-->
                            <!--<span>系统维护</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
        </ul>
    </div>
    <div class="ps-scrollbar-x-rail" style="width: 235px; display: none; left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 592px; display: inherit; right: 0px;"><div class="ps-scrollbar-y" style="top: 0px; height: 545px;"></div></div></div>
<div class="aw-content-wrap">
    <div class="mod">
        <div class="mod-head">
            <ul class="nav nav-tabs">
                <li class="active"><a href="">职位列表</a></li>
                <li><a href="<?php echo U('Admin/Job/add');?>" >添加职位</a></li>
            </ul>
        </div>

        <div class="tab-content mod-body">
            <div class="tab-pane active" id="list">
                <div class="table-responsive">

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>职位名称</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <form action="<?php echo U('Admin/Job/edit');?>" method="post">
                        <!--<form id="jobs_form" action="<?php echo U('Admin/Job/edit');?>" method="post" onsubmit="return false">-->
                            <?php if(is_array($jobData)): foreach($jobData as $key=>$vo): ?><tr>
                                    <td><?php echo ($vo['id']); ?></td>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input class="job-title form-control" type="text" name="job_list[<?php echo ($vo['id']); ?>]" value="<?php echo ($vo['name']); ?>">
                                            </div>
                                        </div>
                                    </td>
                                    <!--<td><a onclick="AWS.dialog(&#39;confirm&#39;, {&#39;message&#39;: &#39;确认删除?&#39;}, function(){AWS.ajax_request(G_BASE_URL + &#39;/admin/ajax/remove_job/&#39;, &#39;id=1&#39;);}); " class="icon icon-trash md-tip" data-toggle="tooltip" title="" data-original-title="删除"></a></td>-->
                                    <td><a href="<?php echo U('Admin/Job/delete',array('id'=>$vo['id']));?>" class="icon icon-trash md-tip" data-toggle="tooltip" title="" data-original-title="删除"></a></td>
                                </tr><?php endforeach; endif; ?>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3">
                                <input type="submit" value="保存设置" class="btn btn-primary center-block">
                            </td>
                        </tr>
                        </tfoot>
                        </form>
                    </table>

                </div>
            </div>

            <!--<div class="tab-pane" id="add">-->
                <!--<div class="table-responsive">-->
                    <!--<form method="post" action="http://127.0.0.1/wenda/?/admin/ajax/add_job/" onsubmit="return false;" id="new_job_form" class="form-horizontal" role="form">-->
                        <!--<div class="form-group">-->
                            <!--<span class="col-sm-2 col-xs-3 control-label">添加新职位:</span>-->
                            <!--<div class="col-sm-5 col-xs-8">-->
                                <!--<textarea class="form-control textarea" name="jobs" rows="5"></textarea>-->

                                <!--<span class="help-block">一行一个职位名称</span>-->
                            <!--</div>-->
                        <!--</div>-->

                        <!--<div class="form-group">-->
                            <!--<div class="col-sm-offset-2 col-sm-5 col-xs-8">-->
                                <!--<button type="button" onclick="AWS.ajax_post($(&#39;#new_job_form&#39;));" class="btn btn-primary">保存设置</button>-->
                            <!--</div>-->
                        <!--</div>-->
                    <!--</form>-->
                <!--</div>-->
            <!--</div>-->
        </div>
    </div>
</div>
<div class="aw-footer">
    <p>Copyright © 2016 - Powered By <a href="http://www.wecenter.com/?copyright" target="blank">WeCenter 3.1.7</a></p>
</div>

<!-- DO NOT REMOVE -->
<div id="aw-ajax-box" class="aw-ajax-box"><div id="aw-loading" class="hide" style="display: none; top: 822.5px; left: 513.5px; position: absolute;"><div id="aw-loading-box" style="background-position: 0px 440px;"></div></div></div>


<div style="display:none;" id="__crond"><img src="./js/saved_resource" width="1" height="1"></div>

<!-- Escape time: 0.15705108642578 --><!-- / DO NOT REMOVE -->



</body></html>