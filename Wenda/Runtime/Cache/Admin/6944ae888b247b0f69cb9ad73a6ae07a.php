<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<!-- saved from url=(0031)http://127.0.0.1/wenda/?/admin/ -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="blank">
    <meta name="format-detection" content="telephone=no">
    <title>概述 -学并思问答</title>
    <!--<base href="http://127.0.0.1/wenda/?/">--><base href=".">
    <link rel="stylesheet" type="text/css" href="/wd/Public/Admin/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/wd/Public/Admin/Css/icon.css" />
    <link rel="stylesheet" type="text/css" href="/wd/Public/Admin/Css/common.css" />


    <script type="text/javascript">
        var G_INDEX_SCRIPT = "?/";
        var G_BASE_URL = "http://127.0.0.1/wenda/?";
        var G_STATIC_URL = "http://127.0.0.1/wenda/static";
        var G_UPLOAD_URL = "http://127.0.0.1/wenda/uploads";
        var G_USER_ID = "1";
        var G_POST_HASH = "";
    </script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/jquery-2.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/aws_admin.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/aws_admin_template.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/jquery-form.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/framework.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/global.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/echarts-data.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/echarts.js"></script>


    <!--[if lte IE 8]>
    <script type="text/javascript" src="http://127.0.0.1/wenda/static/js/respond.js"></script>
    <![endif]-->
</head>

<body>
<div class="aw-header">
    <button class="btn btn-sm mod-head-btn pull-left">
        <i class="icon icon-bar"></i>
    </button>

    <div class="mod-header-user">
        <ul class="pull-right">

            <li class="dropdown">
                <a href="http://127.0.0.1/wenda/?/#" class="dropdown-toggle mod-bell" data-toggle="dropdown">
                    <i class="icon icon-bell"></i>
                </a>
                <ul class="dropdown-menu mod-chat">
                    <p>没有通知</p>
                </ul>
            </li>

            <li class="dropdown username">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/wd<?php echo (session('face_url')); ?>" class="img-circle" width="30"><?php echo (session('username')); ?>
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu pull-right mod-user">
                    <li>
                        <a href="http://127.0.0.1/wenda" target="_blank"><i class="icon icon-home"></i>首页</a>
                    </li>

                    <li>
                        <a href="<?php echo U('Admin/index/index');?>"><i class="icon icon-ul"></i>概述</a>
                    </li>

                    <li>
                        <a href="<?php echo U('Admin/Login/logout');?>"><i class="icon icon-logout"></i>退出</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<div class="aw-side ps-container" id="aw-side">
    <div class="mod">
        <div class="mod-logo">
            <img class="pull-left" src="/wd/Public/Admin/images/logo.png" alt="">
            <h1>WeCenter</h1>
        </div>

        <div class="mod-message">
            <div class="message">
                <a class="btn btn-sm" href="http://127.0.0.1/wenda" target="_blank" title="首页">
                    <i class="icon icon-home"></i>
                </a>

                <a class="btn btn-sm" href="./images/index.html" title="概述">
                    <i class="icon icon-ul"></i>
                </a>

                <a class="btn btn-sm" href="<?php echo U('Admin/Login/logout');?>" title="退出">
                    <i class="icon icon-logout"></i>
                </a>
            </div>
        </div>

        <ul class="mod-bar">
            <input type="hidden" id="hide_values" val="0">
            <li>
                <a href="<?php echo U('Admin/Index/index');?>" class=" icon icon-home active">
                    <span>概述</span>
                </a>
            </li>
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-setting" data="icon">-->
                    <!--<span>全局设置</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-site">-->
                            <!--<span>站点信息</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-register">-->
                            <!--<span>注册访问</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-functions">-->
                            <!--<span>站点功能</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-contents">-->
                            <!--<span>内容设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-integral">-->
                            <!--<span>威望积分</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-permissions">-->
                            <!--<span>用户权限</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-mail">-->
                            <!--<span>邮件设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-openid">-->
                            <!--<span>开放平台</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-cache">-->
                            <!--<span>性能优化</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-interface">-->
                            <!--<span>界面设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <li>
                <a href="javascript:;" class=" icon icon-reply" data="icon">
                    <span>内容管理</span>
                </a>

                <ul class="hide">
                    <li>
                        <a href="<?php echo U('Admin/Question/index');?>">
                            <span>问题管理</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo U('Admin/Category/index');?>">
                            <span>回复管理</span>
                        </a>
                    </li>
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/article/list/">-->
                            <!--<span>文章管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/topic/list/">-->
                            <!--<span>话题管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <li>
                        <a href="<?php echo U('Admin/Category/index');?>">
                        <span>分类管理</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" class=" icon icon-user" data="icon">
                    <span>用户管理</span>
                </a>

                <ul class="hide">
                    <li>
                        <a href="<?php echo U('Admin/User/index');?>">
                            <span>用户列表</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://127.0.0.1/wenda/?/admin/user/group_list/">
                            <span>用户组</span>
                        </a>
                    </li>
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/invites/">-->
                            <!--<span>批量邀请</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <li>
                        <a href="<?php echo U('Admin/Job/index');?>">
                            <span>职位设置</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-report" data="icon">-->
                    <!--<span>审核管理</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/approval/list/">-->
                            <!--<span>内容审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/verify_approval_list/">-->
                            <!--<span>认证审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/register_approval_list/">-->
                            <!--<span>注册审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/question/report_list/">-->
                            <!--<span>用户举报</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-signup" data="icon">-->
                    <!--<span>内容设置</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/nav_menu/">-->
                            <!--<span>导航设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/category/list/">-->
                            <!--<span>分类管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/feature/list/">-->
                            <!--<span>专题管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/page/">-->
                            <!--<span>页面管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/help/list/">-->
                            <!--<span>帮助中心</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-share" data="icon">-->
                    <!--<span>微信微博</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/accounts/">-->
                            <!--<span>微信多账号管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/mp_menu/">-->
                            <!--<span>微信菜单管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/reply/">-->
                            <!--<span>微信自定义回复</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/third_party_access/">-->
                            <!--<span>微信第三方接入</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/qr_code/">-->
                            <!--<span>微信二维码管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/sent_msgs_list/">-->
                            <!--<span>微信消息群发</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weibo/msg/">-->
                            <!--<span>微博消息接收</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/receiving_list/">-->
                            <!--<span>邮件导入</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-inbox" data="icon">-->
                    <!--<span>邮件群发</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/tasks/">-->
                            <!--<span>任务管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/groups/">-->
                            <!--<span>用户群管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-job" data="icon">-->
                    <!--<span>工具</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/tools/">-->
                            <!--<span>系统维护</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
        </ul>
    </div>
    <div class="ps-scrollbar-x-rail" style="width: 235px; display: none; left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 592px; display: inherit; right: 0px;"><div class="ps-scrollbar-y" style="top: 0px; height: 545px;"></div></div></div>
<div class="aw-content-wrap" id="user_list">
    <div class="mod">
        <div class="mod-head">
            <h3>
                <ul class="nav nav-tabs">
                    <li ><a href="<?php echo U('admin/category/index');?>">分类列表</a></li>
                    <li class="active"><a href="">添加分类</a></li>
                </ul>
            </h3>
        </div>
        <div class="tab-content mod-content">
            <div class="tab-pane active" id="add">
                <div class="table-responsive">
                    <form action="<?php echo U('admin/category/add');?>" id="settings_form" method="post">
                        <table class="table table-striped">
                            <tbody><tr>
                                <td>
                                    <div class="form-group">
                                        <span class="col-sm-4 col-xs-3 control-label">分类名:</span>
                                        <div class="col-sm-5 col-xs-8">
                                            <input class="form-control" name="category_name" type="text" value="">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <span class="col-sm-4 col-xs-3 control-label">所属分类:</span>
                                        <div class="col-sm-5 col-xs-8">
                                            <select class="form-control" name="fid">
                                                <option value="0">顶级分类</option>
                                                <?php if(is_array($cateData)): foreach($cateData as $key=>$vo): ?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["category_name"]); ?></option><?php endforeach; endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            </tbody><tfoot>
                        <tr>
                            <td>
                                <input type="submit" value="添加分类" class="btn btn-primary center-block">
                            </td>
                        </tr>
                        </tfoot>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="aw-footer">
    <p>Copyright © 2016 - Powered By <a href="http://www.wecenter.com/?copyright" target="blank">WeCenter 3.1.7</a></p>
</div>

<!-- DO NOT REMOVE -->
<div id="aw-ajax-box" class="aw-ajax-box"><div id="aw-loading" class="hide" style="display: none; top: 822.5px; left: 513.5px; position: absolute;"><div id="aw-loading-box" style="background-position: 0px 440px;"></div></div></div>


<div style="display:none;" id="__crond"><img src="./js/saved_resource" width="1" height="1"></div>

<!-- Escape time: 0.15705108642578 --><!-- / DO NOT REMOVE -->



</body></html>