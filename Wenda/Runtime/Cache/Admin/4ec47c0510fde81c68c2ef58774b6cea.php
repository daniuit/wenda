<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<!-- saved from url=(0031)http://127.0.0.1/wenda/?/admin/ -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="blank">
    <meta name="format-detection" content="telephone=no">
    <title>概述 -学并思问答</title>
    <!--<base href="http://127.0.0.1/wenda/?/">--><base href=".">
    <link rel="stylesheet" type="text/css" href="/wd/Public/Admin/Css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/wd/Public/Admin/Css/icon.css" />
    <link rel="stylesheet" type="text/css" href="/wd/Public/Admin/Css/common.css" />


    <script type="text/javascript">
        var G_INDEX_SCRIPT = "?/";
        var G_BASE_URL = "http://127.0.0.1/wenda/?";
        var G_STATIC_URL = "http://127.0.0.1/wenda/static";
        var G_UPLOAD_URL = "http://127.0.0.1/wenda/uploads";
        var G_USER_ID = "1";
        var G_POST_HASH = "";
    </script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/jquery-2.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/aws_admin.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/aws_admin_template.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/jquery-form.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/framework.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/global.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/echarts-data.js"></script>
    <script type="text/javascript" src="/wd/Public/Admin/Js/echarts.js"></script>


    <!--[if lte IE 8]>
    <script type="text/javascript" src="http://127.0.0.1/wenda/static/js/respond.js"></script>
    <![endif]-->
</head>

<body>
<div class="aw-header">
    <button class="btn btn-sm mod-head-btn pull-left">
        <i class="icon icon-bar"></i>
    </button>

    <div class="mod-header-user">
        <ul class="pull-right">

            <li class="dropdown">
                <a href="http://127.0.0.1/wenda/?/#" class="dropdown-toggle mod-bell" data-toggle="dropdown">
                    <i class="icon icon-bell"></i>
                </a>
                <ul class="dropdown-menu mod-chat">
                    <p>没有通知</p>
                </ul>
            </li>

            <li class="dropdown username">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/wd<?php echo (session('face_url')); ?>" class="img-circle" width="30"><?php echo (session('username')); ?>
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu pull-right mod-user">
                    <li>
                        <a href="http://127.0.0.1/wenda" target="_blank"><i class="icon icon-home"></i>首页</a>
                    </li>

                    <li>
                        <a href="<?php echo U('Admin/index/index');?>"><i class="icon icon-ul"></i>概述</a>
                    </li>

                    <li>
                        <a href="<?php echo U('Admin/Login/logout');?>"><i class="icon icon-logout"></i>退出</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<div class="aw-side ps-container" id="aw-side">
    <div class="mod">
        <div class="mod-logo">
            <img class="pull-left" src="/wd/Public/Admin/images/logo.png" alt="">
            <h1>WeCenter</h1>
        </div>

        <div class="mod-message">
            <div class="message">
                <a class="btn btn-sm" href="http://127.0.0.1/wenda" target="_blank" title="首页">
                    <i class="icon icon-home"></i>
                </a>

                <a class="btn btn-sm" href="./images/index.html" title="概述">
                    <i class="icon icon-ul"></i>
                </a>

                <a class="btn btn-sm" href="<?php echo U('Admin/Login/logout');?>" title="退出">
                    <i class="icon icon-logout"></i>
                </a>
            </div>
        </div>

        <ul class="mod-bar">
            <input type="hidden" id="hide_values" val="0">
            <li>
                <a href="<?php echo U('Admin/Index/index');?>" class=" icon icon-home active">
                    <span>概述</span>
                </a>
            </li>
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-setting" data="icon">-->
                    <!--<span>全局设置</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-site">-->
                            <!--<span>站点信息</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-register">-->
                            <!--<span>注册访问</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-functions">-->
                            <!--<span>站点功能</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-contents">-->
                            <!--<span>内容设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-integral">-->
                            <!--<span>威望积分</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-permissions">-->
                            <!--<span>用户权限</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-mail">-->
                            <!--<span>邮件设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-openid">-->
                            <!--<span>开放平台</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-cache">-->
                            <!--<span>性能优化</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/settings/category-interface">-->
                            <!--<span>界面设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <li>
                <a href="javascript:;" class=" icon icon-reply" data="icon">
                    <span>内容管理</span>
                </a>

                <ul class="hide">
                    <li>
                        <a href="<?php echo U('Admin/Question/index');?>">
                            <span>问题管理</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo U('Admin/Category/index');?>">
                            <span>回复管理</span>
                        </a>
                    </li>
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/article/list/">-->
                            <!--<span>文章管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/topic/list/">-->
                            <!--<span>话题管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <li>
                        <a href="<?php echo U('Admin/Category/index');?>">
                        <span>分类管理</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;" class=" icon icon-user" data="icon">
                    <span>用户管理</span>
                </a>

                <ul class="hide">
                    <li>
                        <a href="<?php echo U('Admin/User/index');?>">
                            <span>用户列表</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://127.0.0.1/wenda/?/admin/user/group_list/">
                            <span>用户组</span>
                        </a>
                    </li>
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/invites/">-->
                            <!--<span>批量邀请</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <li>
                        <a href="<?php echo U('Admin/Job/index');?>">
                            <span>职位设置</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-report" data="icon">-->
                    <!--<span>审核管理</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/approval/list/">-->
                            <!--<span>内容审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/verify_approval_list/">-->
                            <!--<span>认证审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/user/register_approval_list/">-->
                            <!--<span>注册审核</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/question/report_list/">-->
                            <!--<span>用户举报</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-signup" data="icon">-->
                    <!--<span>内容设置</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/nav_menu/">-->
                            <!--<span>导航设置</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/category/list/">-->
                            <!--<span>分类管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/feature/list/">-->
                            <!--<span>专题管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/page/">-->
                            <!--<span>页面管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/help/list/">-->
                            <!--<span>帮助中心</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-share" data="icon">-->
                    <!--<span>微信微博</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/accounts/">-->
                            <!--<span>微信多账号管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/mp_menu/">-->
                            <!--<span>微信菜单管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/reply/">-->
                            <!--<span>微信自定义回复</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/third_party_access/">-->
                            <!--<span>微信第三方接入</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/qr_code/">-->
                            <!--<span>微信二维码管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weixin/sent_msgs_list/">-->
                            <!--<span>微信消息群发</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/weibo/msg/">-->
                            <!--<span>微博消息接收</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/receiving_list/">-->
                            <!--<span>邮件导入</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-inbox" data="icon">-->
                    <!--<span>邮件群发</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/tasks/">-->
                            <!--<span>任务管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/edm/groups/">-->
                            <!--<span>用户群管理</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
            <!--<li>-->
                <!--<a href="javascript:;" class=" icon icon-job" data="icon">-->
                    <!--<span>工具</span>-->
                <!--</a>-->

                <!--<ul class="hide">-->
                    <!--<li>-->
                        <!--<a href="http://127.0.0.1/wenda/?/admin/tools/">-->
                            <!--<span>系统维护</span>-->
                        <!--</a>-->
                    <!--</li>-->
                <!--</ul>-->
            <!--</li>-->
        </ul>
    </div>
    <div class="ps-scrollbar-x-rail" style="width: 235px; display: none; left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 592px; display: inherit; right: 0px;"><div class="ps-scrollbar-y" style="top: 0px; height: 545px;"></div></div></div>
<div class="aw-content-wrap">
    <div class="mod">
        <div class="mod-head">
            <h3>
                <ul class="nav nav-tabs">
                    <li ><a href="<?php echo U('Admin/Question/index');?>" >问题列表</a></li>
                    <li class="active"><a href="">搜索</a></li>
                </ul>
            </h3>
        </div>
        <div class="mod-body tab-content">
            <!--<div class="tab-pane active" id="list">-->

                <!--<form id="batchs_form" action="http://127.0.0.1/wenda/?/admin/ajax/question_manage/" method="post">-->
                    <!--<input type="hidden" id="action" name="action" value="">-->
                    <!--<div class="table-responsive">-->
                        <!--<table class="table table-striped">-->
                            <!--<thead>-->
                            <!--<tr>-->
                                <!--<th><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" class="check-all" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></th>-->
                                <!--<th>问题标题</th>-->
                                <!--<th>回答</th>-->
                                <!--<th>关注</th>-->
                                <!--<th>浏览</th>-->
                                <!--<th>作者</th>-->
                                <!--<th>发布时间</th>-->
                                <!--<th>最后更新</th>-->
                                <!--<th>操作</th>-->
                            <!--</tr>-->
                            <!--</thead>-->
                            <!--<tbody>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="17" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/17" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>14 秒前</td>-->
                                <!--<td>14 秒前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/17" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="16" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/16" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>37 秒前</td>-->
                                <!--<td>37 秒前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/16" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="15" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/15" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>52 秒前</td>-->
                                <!--<td>52 秒前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/15" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="14" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/14" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>1 分钟前</td>-->
                                <!--<td>1 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/14" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="13" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/13" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>1 分钟前</td>-->
                                <!--<td>1 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/13" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="12" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/12" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>1 分钟前</td>-->
                                <!--<td>1 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/12" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="11" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/11" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/11" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="10" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/10" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/10" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="9" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/9" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/9" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="8" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/8" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/8" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="7" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/7" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td>2 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/7" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="6" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/6" target="_blank">枯基本原则基本原则枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>3 分钟前</td>-->
                                <!--<td>3 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/6" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="5" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/5" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>3 分钟前</td>-->
                                <!--<td>3 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/5" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="4" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/4" target="_blank">枯基本原则基本原则枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>3 分钟前</td>-->
                                <!--<td>3 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/4" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="3" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/3" target="_blank">枯基本原则基本原则枯基本原则基本原则</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>3 分钟前</td>-->
                                <!--<td>3 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/3" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="2" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/2" target="_blank">枯基本原则基本原则</a></td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td>3</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/zhibinm" target="_blank">zhibinm</a></td>-->
                                <!--<td>3 天前</td>-->
                                <!--<td>1 分钟前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/2" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" name="question_ids[]" value="1" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/question/1" target="_blank">这个可以有吗</a></td>-->
                                <!--<td>0</td>-->
                                <!--<td>1</td>-->
                                <!--<td>1</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/people/leoma" target="_blank">leoma</a></td>-->
                                <!--<td>3 天前</td>-->
                                <!--<td>3 天前</td>-->
                                <!--<td><a href="http://127.0.0.1/wenda/?/publish/1" target="_blank" class="icon icon-edit md-tip" title="" data-original-title="编辑"></a></td>-->
                            <!--</tr>-->
                            <!--</tbody>-->
                        <!--</table>-->
                    <!--</div>-->
                <!--</form>-->
                <!--<div class="mod-table-foot">-->

                    <!--<a class="btn btn-danger" onclick="$(&#39;#action&#39;).val(&#39;remove&#39;); AWS.ajax_post($(&#39;#batchs_form&#39;));">删除</a>-->
                <!--</div>-->
            <!--</div>-->

            <div class="tab-pane active" id="search">
                <form method="post" action="./images/question_list.html" onsubmit="return false;" id="search_form" class="form-horizontal" role="form">

                    <input name="action" type="hidden" value="search">

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">关键词:</label>

                        <div class="col-sm-5 col-xs-8">
                            <input class="form-control" type="text" value="" name="keyword">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">分类:</label>

                        <div class="col-sm-5 col-xs-8">
                            <select name="category_id" class="form-control">
                                <option value="0"></option>
                                <option value="1">默认分类</option><option value="2">产品分类</option><option value="4">-- 社区分类</option><option value="3">知识问答</option>							</select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">发起时间范围:</label>

                        <div class="col-sm-6 col-xs-9">
                            <div class="row">
                                <div class="col-xs-11  col-sm-5 mod-double">
                                    <input type="text" class="form-control mod-data" value="" name="start_date"><div class="date_selector" style="display: none;"><div class="nav"><p class="month_nav"><span class="buttonx prev" title="[Page-Up]">«</span> <span class="month_name">一月</span> <span class="buttonx next" title="[Page-Down]">»</span></p><p class="year_nav"><span class="buttonx prev" title="[Ctrl+Page-Up]">«</span> <span class="year_name">2016</span> <span class="buttonx next" title="[Ctrl+Page-Down]">»</span></p></div><table><thead><tr><th>一</th><th>二</th><th>三</th><th>四</th><th>五</th><th>六</th><th>日</th></tr></thead><tbody><tr><td class="unselected_month" date="2015-12-27">27</td><td class="unselected_month" date="2015-12-28">28</td><td class="unselected_month" date="2015-12-29">29</td><td class="unselected_month" date="2015-12-30">30</td><td class="unselected_month" date="2015-12-31">31</td><td class="selectable_day" date="2016-01-01">1</td><td class="selectable_day" date="2016-01-02">2</td></tr><tr><td class="selectable_day" date="2016-01-03">3</td><td class="selectable_day" date="2016-01-04">4</td><td class="selectable_day" date="2016-01-05">5</td><td class="selectable_day" date="2016-01-06">6</td><td class="selectable_day" date="2016-01-07">7</td><td class="selectable_day" date="2016-01-08">8</td><td class="selectable_day" date="2016-01-09">9</td></tr><tr><td class="selectable_day" date="2016-01-10">10</td><td class="selectable_day" date="2016-01-11">11</td><td class="selectable_day" date="2016-01-12">12</td><td class="selectable_day" date="2016-01-13">13</td><td class="selectable_day" date="2016-01-14">14</td><td class="selectable_day" date="2016-01-15">15</td><td class="selectable_day" date="2016-01-16">16</td></tr><tr><td class="selectable_day" date="2016-01-17">17</td><td class="selectable_day" date="2016-01-18">18</td><td class="selectable_day today selected" date="2016-01-19">19</td><td class="selectable_day" date="2016-01-20">20</td><td class="selectable_day" date="2016-01-21">21</td><td class="selectable_day" date="2016-01-22">22</td><td class="selectable_day" date="2016-01-23">23</td></tr><tr><td class="selectable_day" date="2016-01-24">24</td><td class="selectable_day" date="2016-01-25">25</td><td class="selectable_day" date="2016-01-26">26</td><td class="selectable_day" date="2016-01-27">27</td><td class="selectable_day" date="2016-01-28">28</td><td class="selectable_day" date="2016-01-29">29</td><td class="selectable_day" date="2016-01-30">30</td></tr><tr><td class="selectable_day" date="2016-01-31">31</td><td class="unselected_month" date="2016-02-01">1</td><td class="unselected_month" date="2016-02-02">2</td><td class="unselected_month" date="2016-02-03">3</td><td class="unselected_month" date="2016-02-04">4</td><td class="unselected_month" date="2016-02-05">5</td><td class="unselected_month" date="2016-02-06">6</td></tr></tbody></table></div>
                                    <i class="icon icon-date"></i>
                                </div>
								<span class="mod-symbol col-xs-1 col-sm-1">
								-
								</span>
                                <div class="col-xs-11 col-sm-5">
                                    <input type="text" class="form-control mod-data" value="" name="end_date"><div class="date_selector" style="display: none;"><div class="nav"><p class="month_nav"><span class="buttonx prev" title="[Page-Up]">«</span> <span class="month_name">一月</span> <span class="buttonx next" title="[Page-Down]">»</span></p><p class="year_nav"><span class="buttonx prev" title="[Ctrl+Page-Up]">«</span> <span class="year_name">2016</span> <span class="buttonx next" title="[Ctrl+Page-Down]">»</span></p></div><table><thead><tr><th>一</th><th>二</th><th>三</th><th>四</th><th>五</th><th>六</th><th>日</th></tr></thead><tbody><tr><td class="unselected_month" date="2015-12-27">27</td><td class="unselected_month" date="2015-12-28">28</td><td class="unselected_month" date="2015-12-29">29</td><td class="unselected_month" date="2015-12-30">30</td><td class="unselected_month" date="2015-12-31">31</td><td class="selectable_day" date="2016-01-01">1</td><td class="selectable_day" date="2016-01-02">2</td></tr><tr><td class="selectable_day" date="2016-01-03">3</td><td class="selectable_day" date="2016-01-04">4</td><td class="selectable_day" date="2016-01-05">5</td><td class="selectable_day" date="2016-01-06">6</td><td class="selectable_day" date="2016-01-07">7</td><td class="selectable_day" date="2016-01-08">8</td><td class="selectable_day" date="2016-01-09">9</td></tr><tr><td class="selectable_day" date="2016-01-10">10</td><td class="selectable_day" date="2016-01-11">11</td><td class="selectable_day" date="2016-01-12">12</td><td class="selectable_day" date="2016-01-13">13</td><td class="selectable_day" date="2016-01-14">14</td><td class="selectable_day" date="2016-01-15">15</td><td class="selectable_day" date="2016-01-16">16</td></tr><tr><td class="selectable_day" date="2016-01-17">17</td><td class="selectable_day" date="2016-01-18">18</td><td class="selectable_day today selected" date="2016-01-19">19</td><td class="selectable_day" date="2016-01-20">20</td><td class="selectable_day" date="2016-01-21">21</td><td class="selectable_day" date="2016-01-22">22</td><td class="selectable_day" date="2016-01-23">23</td></tr><tr><td class="selectable_day" date="2016-01-24">24</td><td class="selectable_day" date="2016-01-25">25</td><td class="selectable_day" date="2016-01-26">26</td><td class="selectable_day" date="2016-01-27">27</td><td class="selectable_day" date="2016-01-28">28</td><td class="selectable_day" date="2016-01-29">29</td><td class="selectable_day" date="2016-01-30">30</td></tr><tr><td class="selectable_day" date="2016-01-31">31</td><td class="unselected_month" date="2016-02-01">1</td><td class="unselected_month" date="2016-02-02">2</td><td class="unselected_month" date="2016-02-03">3</td><td class="unselected_month" date="2016-02-04">4</td><td class="unselected_month" date="2016-02-05">5</td><td class="unselected_month" date="2016-02-06">6</td></tr></tbody></table></div>
                                    <i class="icon icon-date"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">作者:</label>

                        <div class="col-sm-5 col-xs-8">
                            <input class="form-control" type="text" value="" name="user_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">回复数:</label>

                        <div class="col-sm-6 col-xs-9">
                            <div class="row">
                                <div class="col-xs-11  col-sm-5 mod-double">
                                    <input type="text" class="form-control" name="answer_count_min" value="">
                                </div>
								<span class="mod-symbol col-xs-1 col-sm-1">
								-
								</span>
                                <div class="col-xs-11 col-sm-5">
                                    <input type="text" class="form-control" name="answer_count_max" value="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">是否有最佳回复:</label>

                        <div class="col-sm-5 col-xs-8">
                            <div class="checkbox mod-padding">
                                <label><div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" value="1" name="best_answer" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> 有最佳回复</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5 col-xs-8">
                            <button type="button" onclick="AWS.ajax_post($(&#39;#search_form&#39;));" class="btn btn-primary">搜索</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="aw-footer">
    <p>Copyright © 2016 - Powered By <a href="http://www.wecenter.com/?copyright" target="blank">WeCenter 3.1.7</a></p>
</div>

<!-- DO NOT REMOVE -->
<div id="aw-ajax-box" class="aw-ajax-box"><div id="aw-loading" class="hide" style="display: none; top: 822.5px; left: 513.5px; position: absolute;"><div id="aw-loading-box" style="background-position: 0px 440px;"></div></div></div>


<div style="display:none;" id="__crond"><img src="./js/saved_resource" width="1" height="1"></div>

<!-- Escape time: 0.15705108642578 --><!-- / DO NOT REMOVE -->



</body></html>