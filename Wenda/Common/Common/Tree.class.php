<?php
namespace Common\Common;
class Tree {
    public $temArr = array();
    public function categorytree($data,$fid=0,$rank=0){
        foreach($data as $k => $v){
            if($v['fid']==$fid){
                $v['category_name']= $this ->_str($rank).$v['category_name'];
                $this -> temArr[]=$v;
                $this -> categorytree($data,$v['id'],$rank+1);
            }
        }
        return $this->temArr;
    }

    public function _str($rank){

        $str = "┠ ";

        for ($i=0; $i < $rank ; $i++) {
            $str .="－－";
        }

        return $str;
    }

}

