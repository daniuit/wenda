<?php
return array(
    'DEFAULT_MODULE'        =>  'Index',  // 默认模块
    'ACTION_SUFFIX'         =>  'Action', // 操作方法后缀
    'DB_TYPE'               =>  'mysql',     // 数据库类型
    'DB_HOST'               =>  '127.0.0.1', // 服务器地址
    'DB_NAME'               =>  'wenda',          // 数据库名
    'DB_USER'               =>  'root',      // 用户名
    'DB_PWD'                =>  '',          // 密码
    'DB_PORT'               =>  '3306',        // 端口
    'DB_PREFIX'             =>  '',    // 数据库表前缀
    'DB_PARAMS'          	=>  array(), // 数据库连接参数
    'DB_DEBUG'  			=>  TRUE, // 数据库调试模式 开启后可以记录SQL日志
    'DB_FIELDS_CACHE'       =>  true,        // 启用字段缓存
    'LAYOUT_ON'             =>  true, // 是否启用布局
    'URL_HTML_SUFFIX'       =>  '',  // URL伪静态后缀设置
);