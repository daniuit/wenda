/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : wenda

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-01-28 07:50:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `answer`
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `qid` int(11) NOT NULL COMMENT '问题ID',
  `fid` int(11) NOT NULL DEFAULT '0' COMMENT '父ID',
  `content` text NOT NULL COMMENT '内容',
  `good` int(7) NOT NULL COMMENT '点赞',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='回答表';

-- ----------------------------
-- Records of answer
-- ----------------------------

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `fid` int(11) NOT NULL DEFAULT '0' COMMENT '父ID',
  `category_name` varchar(20) NOT NULL COMMENT '分类',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of category
-- ----------------------------

-- ----------------------------
-- Table structure for `favorite`
-- ----------------------------
DROP TABLE IF EXISTS `favorite`;
CREATE TABLE `favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `qid` int(11) NOT NULL COMMENT '问题ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收藏表';

-- ----------------------------
-- Records of favorite
-- ----------------------------

-- ----------------------------
-- Table structure for `focus`
-- ----------------------------
DROP TABLE IF EXISTS `focus`;
CREATE TABLE `focus` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(11) NOT NULL COMMENT '关注的用户ID',
  `fid` int(11) NOT NULL COMMENT '被关注的用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='关注表';

-- ----------------------------
-- Records of focus
-- ----------------------------

-- ----------------------------
-- Table structure for `group`
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_name` varchar(20) NOT NULL COMMENT '分组名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='分组表';

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES ('1', '超级管理员');
INSERT INTO `group` VALUES ('2', '普通成员');

-- ----------------------------
-- Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(25) NOT NULL DEFAULT '' COMMENT '职位名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES ('1', '站长');
INSERT INTO `jobs` VALUES ('2', '产品经理');
INSERT INTO `jobs` VALUES ('3', '视觉设计');
INSERT INTO `jobs` VALUES ('4', '职业经理人');
INSERT INTO `jobs` VALUES ('5', '媒体');

-- ----------------------------
-- Table structure for `question`
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `title` varchar(80) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `cid` int(7) NOT NULL COMMENT '分类ID',
  `views` int(7) NOT NULL COMMENT '浏览次数',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of question
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(25) NOT NULL DEFAULT '' COMMENT '用户',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `gid` int(11) NOT NULL DEFAULT '2' COMMENT '分组ID',
  `turename` varchar(25) NOT NULL DEFAULT '' COMMENT '用户真实名',
  `face_url` varchar(80) NOT NULL DEFAULT '' COMMENT '用户头像',
  `birthday` date NOT NULL COMMENT '用户生日',
  `province` varchar(15) NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(10) NOT NULL DEFAULT '' COMMENT '城市',
  `job` varchar(10) NOT NULL DEFAULT '' COMMENT '职位',
  `introduce` varchar(200) NOT NULL DEFAULT '' COMMENT '自我介绍',
  `qq` int(11) NOT NULL COMMENT 'qq',
  `phone` varchar(11) NOT NULL COMMENT '手机号',
  `mail` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `site` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `sex` enum('男','女','保密') NOT NULL DEFAULT '保密',
  `age` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin888', '123456', '2', '', '', '0000-00-00', '', '', '', '', '0', '0', '', '', '保密', '0');
INSERT INTO `user` VALUES ('11', 'zhibinm', 'e10adc3949ba59abbe56e057f20f883e', '1', '马大大', './Uploads/avatars/20160127/56a86d21e1617.jpg', '1986-11-30', '内蒙古', '鄂尔多斯', '职业经理人', '一个小小的人', '6955044', '18925139194', '6955044@qq.com', '', '女', '0');
INSERT INTO `user` VALUES ('12', 'xiaoma', 'e10adc3949ba59abbe56e057f20f883e', '2', '小马哥', './Uploads/avatars/20160127/56a8701f6a84b.jpg', '1905-05-08', '内蒙古', '鄂尔多斯', '视觉设计', '地板砖sd', '89045645', '6456456456', '43345345@qq.com', '46456', '男', '0');
